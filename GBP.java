import java.util.*;
import java.io.*;
/**
 *Gideon Ojo
 *CSC 205 
 *Project #2
 */
public class GBP
{
	public static void main(String [] args)
	{
		//Begin by clearing the screen and creating 
		//an ArrayList to store all LibraryBook objects
		clearScreen();
		String fileName = openingPage();
		ArrayList<LibraryBook> books = new ArrayList<LibraryBook>();
		//Input all the book information 
		input(fileName, books);
		//Go to main menu
		menu(books);
	}//End of main

/*Methods*/
	//Mehtod prints the information each Library Book contains
	//Return: void.		paramaters: ArrayList of LinraryBooks, element(position) of book.
	private static void printRecord(ArrayList<LibraryBook> book, int element)
	{
		clearScreen();
		System.out.println("Book # " + (element + 1));
		System.out.println("\n" + "Title:" + "\t" + "\t" + "\t" + book.get(element).getTitle());
		System.out.println("Author's Name:" + "\t" + "\t" + book.get(element).getAuthor());
		System.out.println("Copyright:" + "\t" + "\t" + book.get(element).getCopyright());
		System.out.println("Price:" + "\t" + "\t" + "\t" + book.get(element).getPrice());
		System.out.println("Genre:" + "\t" + "\t" + "\t" + book.get(element).getGenre());
		enterCont();
	}
	//Method runs through loop to print ALL of the books in a file
	//Return: void.		parameter: ArrayList of LibraryBooks
	private static void displayRecords(ArrayList<LibraryBook> book)
	{
		for(int i = 0; i < book.size(); i++)
		{
			printRecord(book, i);
		}
	}
	//Simple method to print out the information for the menu
	private static void printList()
	{
		System.out.println("\n" + "BOOK SEARCH PROGRAM");
                System.out.println("___________________________________________");
                System.out.println("\n" + "1) Display all books recorded");
                System.out.println("2) Search for a book by title");
                System.out.println("3) Exit");
                System.out.println("___________________________________________");
                System.out.print("Enter option: ");

	}
	//Main menu where most of the program is run.
	//Takes user input from 1-3 and uses switch case to return their given command.
	//Loops until user chooses to exit (select #3)
	private static void menu(ArrayList<LibraryBook> books)
	{
		printList();
		Scanner in = new Scanner(System.in);
		int option = 0;
		while(option!= 3)
		{
			option = in.nextInt();
			int pick;	//Stores the location of searched book
			switch(option)
			{
				case 1: displayRecords(books);
					printList();
					break;
				case 2: pick = findBook(books);
					if(pick == -1){
						System.out.println("Sorry, book was not found..");
						System.out.print("Enter option: ");}
					else{
						printRecord(books, pick);
						clearScreen();
						printList();}
					break;
				case 3: clearScreen();
					System.out.println("Goodbye!");
					break;
				default: clearScreen();	
					printList();
					System.out.println("That is not an option. PLease try again.");
					System.out.print("Enter option: ");
					break;
			}
		}
	}
	//THis method is used to print the files availible in the beginning
	//of the program. Grabs all .dat files from directory
	public static String bookFiles()
	{
		//Get all files from directory
		File curDir = new File(".");
                String[] fileNames = curDir.list();
                ArrayList<String> data = new ArrayList<String>();
		//Find files using the .dat format
		for(String s:fileNames)
                        if(s.endsWith(".dat"))
                                data.add(s);

		//Print files that have .dat format
		String files = "";
		for(String element: data)
		{
			files += (element + "  ");
		}
		return files;
	}
	//Selection Sort to sort all of the LibraryBooks in the ArrayList
	//by their Title
	private static void sortBooks(ArrayList<LibraryBook> book)
	{
		int minIndex, index, j;
		LibraryBook temp;

		for(index = 0; index < book.size() - 1; index++)
		{
			minIndex = index;
			for(j = minIndex+1; j < book.size(); j++)
			{
				if(book.get(j).getTitle().compareTo(book.get(minIndex).getTitle()) < 0)
					minIndex = j;
			}
			if(minIndex != index)
			{
				temp = book.get(index);
				book.set(index, book.get(minIndex));
				book.set(minIndex, temp);
			}
		}
	}
	//Take user input for book title, then search using a 
	//binarySearch. If book is not found, -1 is returned.
	//If book is found, its location in the arrayList is returned.
	private static int findBook(ArrayList<LibraryBook> book)
	{
		int element = 0; String title = "";
		System.out.print("Enter book title: ");
		Scanner in = new Scanner(System.in);
		title = in.nextLine();

		int first = 0; 
		int last = book.size() - 1;
		int  middle;
		boolean found = false;

		do
		{
			middle = (first + last) / 2;
			if((book.get(middle)).getTitle().compareTo(title) == 0)
				found = true;
			else if(book.get(middle).getTitle().compareTo(title) > 0)
				last = middle - 1;
			else
				first = middle + 1;

		} while( (!found) && (first <= last) );
		
		element = middle;
		
		return (found ? element : -1);
	}
	//Clears the entire window
	private static void clearScreen()
        {
       		 System.out.println("\u001b[H\u001b[2J");
        }
	//Inputs each title, author, etc into a LibraryBook that 
	//will be stored in our ArrayList
	public static int input(String fileName, ArrayList<LibraryBook> book)
	{
		int num = 0;
		try{
			Scanner scan = new Scanner(new File(fileName));
			while(scan.hasNext())
			{
				Scanner in = new Scanner(scan.nextLine()).useDelimiter(";");
				String title = in.next();
				String name = in.next();
				int copyright = in.nextInt();
				double price = in.nextDouble();
				String genre = in.next();

				book.add(new LibraryBook(title, name, copyright, price, genre));
				
				num ++;
			}
			
		}
		catch(IOException e) {
			System.out.println(e.getMessage()); }

		sortBooks(book);
		System.out.println("\n" + "There are a total of " + num + " books in your file.");
		enterCont();
		clearScreen();
		return num;	
	}
	//Prompts user to hit return to continue 
	private static void enterCont()
	{
		System.out.print("\n" + "Please press ENTER to continue...");
		Scanner scann = new Scanner(System.in);
                scann.nextLine();
		
		clearScreen();
	}	
	//Display page of program. Shows what files can be searched
	//and returns the file the user wants to sort
	public static String openingPage()
	{
		clearScreen();
		System.out.println("\n" + "\t" + "\t" + "\tWELCOME TO THE GREAT BOOKS PROGRAM");
                System.out.println("-------------------------------------------------------------------------------");
                System.out.println("\n" + "\t" + "What file are your books stored in?");
                System.out.println("\n" + "\t" + "Below are the files stored in the current directory:");
		System.out.println("\n" + bookFiles());
		System.out.print("\n" + "FILE: ");
		Scanner scan = new Scanner(System.in);
		String result = scan.nextLine();
	
		return result;
	}
}// End of GBP
