Gideon Ojo

Friday, October 13th

CSC 205: Project #2

	+++The Great Books Program+++

//Problem Description //
There are file(s) that contains up to 50 books and given information about them such as author and genre.
We are tasked with creating a program that will take this file and sort and organize all of the titles.
A book will go from a sample input of "Tom Sawyer; Twain, Mark; 1972; 8.50;f; to an output such as
Title:	Tom Sawyer
Author:	Twain, Mark
Copyright: ... 
When the program is complete, users will be able to input the collection of books. The program will then display a 
menu that ask them if they want to Display all books or search for a book (by title). If the user does not want to
do either option, they can exit the prorgam. The two different algorithms being used in this program will be the
SELECTION SORT and the BINARY SEARCH
-The Selection sort works by comparing 2 elements in a set and swapping them to place the smallest element in the front
Ex: 
5 2 1 4 3 (No sort)
1 2 5 4 3 (1 Pass)
1 2 5 4 3 (2 Pass)
1 2 3 4 5 (3 Pass) -> Elements now sorted.
-The Binary search works by modeling a human search on a sorted list. This means that it will go to the middle of a list
and if there is no match, go up or down and search again within a smaller window. This search is logarithmic and much
faster than a linear search.
Ex: (Searching for #5)
0 1 [2] 3 4 5	(Check #1 from 0 to 5)
0 1 2 3 [4] 5	(CHeck #2 from 3 to 5)
0 1 2 3 4 [5]	(Check #3 from 5 to 5)	-> Element found.
--------------------------------------------------------------------------------------------------------------------
//Program Specification //
The data being input to the program will come from a .dat file the user specifies. The information from this file will be imported and a LibraryBook object will be created for each line of text. Each object will be stored in an arraylist which can be accessed easily in the program. The written program will then output the library data in text where the user specifies.
--------------------------------------------------------------------------------------------------------------------
//Program Algorithm //
Steps:
1.	Begin with creating a LibraryBook class and a sample .dat file for later use.
2.	Give the LibraryBook class attributes that can be used in the Great Books Program
3.	Create GBP file. Will include main, input, menu, search, display, and print methods
4.	Make sure to create ArrayList to store LibraryBooks in the main method
5.	Create the different methods
	-Input method: Takes the .dat file and inputs all the book information.
	-Menu method: Displays menu to user, and returns their selected option.
	-Search method: Find's book TITLE that user ask for. 
	-Display method: Displays all of the books in the "Library"
	-Print method: Displays the book that is specified
--------------------------------------------------------------------------------------------------------------------
//Note's //
Create LibraryBook Class that can store all informatiion of a book (Authour, Genre, Date, etc)
- Will need to create an Arrray or ArrayList of this object LibraryBook

Display all database files (.dat) . Prompt user for file with books. 

Input method: Input all the fields of book. Assume input format will be the same.
	*Use while loop instead of for (don't know how many books)
	*Books then need to be sorted and the amount of books should be dispplayed
	*Have user leave method by hitting return.
	**Sort with SelectionSort(in class). Only sort using book title.

Menu method: Displays menu information. Takes user input and returns in to the main method
	*Gives user an error(to try again) if int is not between 1 & 3


Main method: Call input(). Call menu(), and use choice in a switch statement.

=Method that promts user for book title

Search method: See's if title is in ArrayList. Use binary search(fastest).
	*If book is found, display information(including location)
	*If not found, inform them book has not been found.

displayRecords method: Uses for loop to call "printRecord" of books.
	*Add code to make user hit return before clearing screen & displaying each record
	*Add code to let user exit method if desired

printRecord method:Displays all book information, invluding its position in the array
--------------------------------------------------------------------------------------------------------------------
